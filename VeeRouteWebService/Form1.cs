﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VeeRouteWebService.RemoteWebService;

namespace VeeRouteWebService
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var point = new addPoint();

            point.providerType = ProviderTypeTextBox.Text;
            point.accountID = accountTextBox.Text;
            point.deviceId = deviceIdTextBox.Text;
            point.latitude = Convert.ToDouble(LatitudeTextBox.Text.Contains(",") ? LatitudeTextBox.Text.Replace(",", ".") : LatitudeTextBox.Text, new CultureInfo("en-US"));
            point.longitude = Convert.ToDouble(LongitudeTextBox.Text.Contains(",") ? LongitudeTextBox.Text.Replace(",", ".") : LongitudeTextBox.Text, new CultureInfo("en-US"));
            point.time = DateTime.UtcNow;

            point.latitudeSpecified = true;
            point.longitudeSpecified = true;
            point.timeSpecified = true;
            point.odometerSpecified = false;
            point.headingSpecified = false;
            point.speedSpecified = false;

            try
            {
                Service.ServiceInstance.addPoint(point);
                logsTextBox.Text += String.Format("[{0}] Point {1}, {2} was sent successfully.\r\n", point.deviceId, LatitudeTextBox.Text, LongitudeTextBox.Text);
            }
            catch (Exception ex)
            {
                logsTextBox.Text += "[error]: ";
                logsTextBox.Text += ex.Message;
                if (ex.InnerException != null)
                {
                    logsTextBox.Text += "\r\n" + ex.InnerException.Message + "\r\n";
                }
            }
        }
    }
}
