﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VeeRouteWebService.RemoteWebService;

namespace VeeRouteWebService
{
    public class Service
    {
        private static RemoteWebService.TrackingWebServiceImplClient _serviceClient = new RemoteWebService.TrackingWebServiceImplClient();
        private Service()
        {
            
        }

        public static TrackingWebServiceImplClient ServiceInstance
        {
            get
            {
                return _serviceClient;
            }
        }
    }
}
